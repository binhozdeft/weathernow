import React, {useEffect, useState} from 'react';
import logo from './icons/logo.svg';
import Search from './components/Search';
import styled, {ThemeProvider} from 'styled-components';
import './App.css';
import Today from './components/Today';
import LastDays from './components/LastDays';
import WeatherGraph from './components/Chart';
import moment from 'moment';
import _ from 'lodash';
import TempGraph from './components/TempMap';

const theme:any = {
  colors: {
    blue: '#1F69B2',
    bluelight: '#DBFBFF',
    gray: '#A7B8C9',
    graylight: '#CDDAE7',
    yellow: '#F4BF02',
    red: '#EC6E4C'
  }
}
function App() {
  const [weatherData, setWeatherData] = useState<any | null>(null);
  const [lastDays, setLastDays] = useState<any | null>(null);
  const [degrees, setDegrees] = useState('C');

  const changeDegrees = (event:any) => {
    if(degrees === 'C'){
      setDegrees('F');
    }else {
      setDegrees('C');
    }
  }
  const updateWeather = (weather: any, list: any) => {
    setWeatherData(weather);
    setLastDays(list);
  }
  const getLastDays = (weather: any) => {
    fetch(`//api.openweathermap.org/data/2.5/forecast?lat=${weather.coord.lat}&lon=${weather.coord.lon}&appid=${process.env.REACT_APP_WEATHER_APIKEY}&units=metric`).then(r => r.json()).then(r => {
      let list:any = r.list.filter((r:any) => moment(r.dt_txt).format('DD/MM/YYYY') !== moment().format('DD/MM/YYYY'));
      list.map((x:any) => {
        x.gref = x.dt_txt.split(' ')[0];
        return x;
      })
      let groupedList = _.groupBy(list, 'gref');
      var filteredList:any = [];
      _.forEach(groupedList, (arr: any) => {
        filteredList.push({min: _.reduce(arr, (prev: any, curr: any) => prev.main.temp_min < curr.main.temp_min ? prev : curr), max:_.reduce(arr, (prev: any, curr: any) => prev.main.temp_max > curr.main.temp_max ? prev : curr)})
      })
      if(filteredList.length > 5){
        filteredList.pop();
      }
      setLastDays(filteredList);
    })
  }
  const getWeather = (value: any, type: string) => {
    let url:string = '';
    if(type === 'address'){
      url = `//api.openweathermap.org/data/2.5/weather?q=${value.keyword}&appid=${process.env.REACT_APP_WEATHER_APIKEY}&units=metric`;
    }else if(type === 'geolocation'){
      url = `//api.openweathermap.org/data/2.5/weather?lat=${value.lat}&lon=${value.long}&appid=${process.env.REACT_APP_WEATHER_APIKEY}&units=metric`;
    };
    fetch(url).then(r => {
      return r.json();
    }).then(r => {
      setWeatherData(r);
      getLastDays(r)
    });
  }
  const showPosition = (position:any) => {
    getWeather({lat: position.coords.latitude, long: position.coords.longitude}, 'geolocation');
  }
  const getGeolocation = () => {
    if(navigator.geolocation){
      navigator.geolocation.getCurrentPosition(
        position => {
          showPosition(position);
        },
        error => {
          getWeather({keyword: 'Belmonte, PT'}, 'address');
        }
      );
    }else {
      getWeather({keyword: 'Belmonte, PT'}, 'address');
    }
  }
  useEffect(() => {
    getGeolocation();
  }, [])
  return (
    <ThemeProvider theme={theme}>
      <Wrapper>
          <SideBar>
            <Header>
              <a href="/"><img src={logo} className="wn-header__logo" alt="WeatherNow" /></a>
              <Button onClick={e => changeDegrees(e)}>º{degrees}</Button>
            </Header>
            <Search updateWeather={updateWeather} />
            <BgIcon>
              {weatherData ? (<img src={`https://openweathermap.org/img/wn/${weatherData.weather[0].icon}@4x.png`} alt={weatherData.weather[0].main} />) : null}
            </BgIcon>
            <Today value={weatherData} unit={degrees} />
            <LastDays unit={degrees} lastDays={lastDays} />
            <BottomText>© 2021 WeatherNow. All rights reserved.</BottomText>
          </SideBar>
          <MainContent>
            <WeatherGraph unit={degrees} lastDays={lastDays} />
            <TempGraph weather={weatherData} />
          </MainContent>
          <Footer>
            <p>© 2021 WeatherNow. All rights reserved.</p>
          </Footer>
      </Wrapper>
    </ThemeProvider>
  );
}

const Wrapper = styled.div`
  width:100%;
  padding:1rem;
  @media screen and (min-width:768px){
    padding:0;
    display:flex;
    background: rgba(245, 250, 255, 0.6);
    box-shadow: -.5rem .5rem 1.5rem -1px #CDDAE7;
    border-radius: .5rem;
    max-width:64rem;
    margin:0 auto;
  }
`;
const SideBar = styled.section`
  @media screen and (min-width:768px){
    width:20rem;
    background: rgba(255, 255, 255, 0.8);
    box-shadow: -.5rem .5rem 1.5rem -1px #CDDAE7;
    border-radius:.5rem 0 0 .5rem;
    position:relative;
  }
  `;
const MainContent = styled.section`
  @media screen and (min-width:768px){
    width:100%;
    display:block;
    overflow:hidden;
    padding:1.5rem;
  }
`;
const BottomText = styled.p`
  position:absolute;
  bottom: 1rem;
  left: 1rem;
  font-size: 0.8rem;
  color:${props => props.theme.colors.blue};
  @media screen and (max-width: 767px){
    display:none;
  }
`;
const Header = styled.header`
  width:100%;
  display:flex;
  align-items:center;
  justify-content:space-between;
  margin-bottom:1rem;
  a {
    display:flex;
    align-items:center;
  }
  @media screen and (min-width:768px){
    width:20rem;
    padding:.5rem 1rem 0;
  }
`;
const Footer = styled.footer`
  margin-top:2rem;
  padding-top:1.5rem;
  border-top:${props => `1px solid ${props.theme.colors.gray}`};
  text-align:center;
  font-size:.8rem;
  color: ${props => props.theme.colors.blue};
  @media screen and (min-width:768px){
    display:none;
  }
`;
const Button = styled.button`
  width: 2.5rem;
  height: 2.5rem;
  border-radius: 2.5rem;
  display:flex;
  align-items:center;
  justify-content:center;
  color:white;
  background-color: ${props => props.theme.colors.blue};
  font-size: 1rem;
  transition: opacity .25s ease-in-out;
  position:relative;
  z-index:3;
  border:0;
  &:active {
    opacity:.6;
  }
`;
const BgIcon = styled.div`
  position:fixed;
  right:0;
  transform:translateY(-30%);
  overflow:hidden;
  width:80%;
  img {
    transform:translateX(25%);
    opacity:.6;
    min-width:100%;
    max-width:320px;
  }
  @media screen and (min-width: 768px){
    position:absolute;
  }
`;
export default App;
