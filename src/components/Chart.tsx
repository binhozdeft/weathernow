import moment from 'moment';
import React, { useReducer, useEffect } from 'react';
import { LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';
import styled from 'styled-components';
import Loading from './Loading';


const WeatherGraph: React.FC<{lastDays: any, unit: any}> = ({lastDays, unit}) =>{
  const [renderCount, forceUpdate] = useReducer(x => x + 1, 0);
  useEffect(() => {
    lastDays && lastDays.map((x:any) => {
      x.day = moment(x.max.dt_txt).format('ddd');
      if(unit === 'F'){
        x.maximum = x.max.main.temp_max * 9/5 + 32;
        x.minimum = x.min.main.temp_min * 9/5 + 32;
      }else {
        x.maximum = x.max.main.temp_max;
        x.minimum = x.min.main.temp_min;
      }
      return x;
    })
    forceUpdate();
  }, [lastDays, unit, forceUpdate])
  return (
    <ChartWrapper>
        {lastDays ? (
          <div>
        <Title>Temperature Graph</Title>
        <Subtitle><Red>• max º{unit}</Red> <Gray>• min º{unit}</Gray></Subtitle>
      <ResponsiveContainer width="108%" height="100%" aspect={2} >
        <LineChart
          data={lastDays}
          width={500}
          height={300}
          margin={{ top: 5, right: 30, left: 20, bottom: 5 }}
          key={renderCount}
        >
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="day" />
            <YAxis />
            <Tooltip />
            <Legend />
            <Line type="monotone" dataKey="maximum" stroke='#EC6E4C' activeDot={{ r: 8 }} />
            <Line type="monotone" dataKey="minimum" stroke="#A7B8C9" />
        </LineChart>
      </ResponsiveContainer>
      </div>
      ) : <Loading />}
    </ChartWrapper>
  )
}

const ChartWrapper = styled.section`
    width:100%;
    background:white;
    text-align:left;
    padding:1.5rem 1rem;
    background: linear-gradient(225.52deg, rgba(255, 255, 255, 0.8) 0%, rgba(255, 255, 255, 0.5) 74.7%);
    box-shadow: -.25rem .25rem 1.5rem -.1px ${props => props.theme.colors.graylight};
    backdrop-filter: blur(.4rem);
    margin-top:1.5rem;
    border-radius: .5rem;
    min-height:15rem;
    position:relative;
    font-size: .8rem;
    line-height: 1.1rem;
    @media screen and (min-width:768px){
      backdrop-filter:none;
      margin-top:0;
      padding:1.5rem;
    }
    .recharts-responsive-container {
      transform:translateX(-2rem);
      .recharts-default-legend {
        display:none;
      }
      .recharts-text {
        fill:${props => props.theme.colors.blue};
      }
    }
  `;
const Title = styled.h2`
  font-size: 0.87rem;
  line-height: 1.3rem;
  color:${props => props.theme.colors.blue};
  text-align:center;
  margin-bottom:1rem;
`;
const Subtitle = styled.div`
  display:flex;
  justify-content:center;
  align-items:center;
  span {
    margin:0 .5rem;
  }
`;
const Red = styled.span`
  font-size:0.75rem;
  color:${props => props.theme.colors.red};
`;
const Gray = styled.span`
  font-size:0.75rem;
  color:${props => props.theme.colors.gray};
`;
export default WeatherGraph;