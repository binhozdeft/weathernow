import React from 'react';

const Degree: React.FC<{value:number, unit: string}> = ({value, unit}) => {
  return (
    <span>{Math.trunc(unit === 'F' ? value * 9/5 + 32 : value)}<i>º{unit}</i></span>
  )
}
export default Degree;