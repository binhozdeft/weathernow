import moment from 'moment';
import React from 'react';
import styled from 'styled-components';
import Degree from './Degree';
import Loading from './Loading';

const LastDays: React.FC<{lastDays: any, unit: string}> = ({lastDays, unit}) => {
  return (
    <LastDaysWrapper>
      {lastDays ? (
        <div>
          <LastDaysList>
            {lastDays.map((m: any, key: number) => <li key={key}>
              <Date>{moment(m.max.dt_txt).format('ddd, MMM Do')}</Date>
              <Humidity>
                <span>
                  <img src={`https://openweathermap.org/img/wn/${m.max.weather[0].icon}.png`} alt={m.max.weather[0].main} />
                </span>
                {m.max.main.humidity}%
              </Humidity>
              <Temp>
                <Degree value={m.max.main.temp_max} unit={unit} />
                <Degree value={m.min.main.temp_min} unit={unit} />
              </Temp>
              </li>)}
          </LastDaysList>
        </div>
      ) : <Loading />}
    </LastDaysWrapper>
  )
}
const LastDaysWrapper = styled.section`
    width:100%;
    background:white;
    text-align:left;
    padding:1.5rem 1rem;
    background: linear-gradient(225.52deg, rgba(255, 255, 255, 0.8) 0%, rgba(255, 255, 255, 0.5) 74.7%);
    box-shadow: -.25rem .25rem 1.5rem -.1px ${props => props.theme.colors.graylight};
    backdrop-filter: blur(.4rem);
    margin-top:1.5rem;
    border-radius: .5rem;
    min-height:15rem;
    position:relative;
    font-size: .8rem;
    line-height: 1.1rem;
    @media screen and (min-width: 768px){
      box-shadow:none;
      backdrop-filter:none;
      margin-top:0;
      padding:1rem;
    }
  `;
  const LastDaysList = styled.ul`
    padding:0;
    margin:0;
    li {
      display:flex;
      align-items:center;
      justify-content:space-between;
      padding:.2rem 0;
    }
  `;
  const Date = styled.span`
    min-width:6.8rem;
    color:${props => props.theme.colors.blue};
    `;
    const Humidity = styled.span`
    display:flex;
    align-items:center;
    color:${props => props.theme.colors.gray};
    width:4.6rem;
    span {
      background:${props => props.theme.colors.graylight};
      width:2.5rem;
      height:2.5rem;
      border-radius:2.5rem;
      margin-right:.5rem;
      img {
        object-fit:cover;
      }
    }
  `;
  const Temp = styled.span`
    color:${props => props.theme.colors.blue};
    display:flex;
    align-items:flex-start;
    justify-content:center;
    span {
      display:flex;
      align-items:flex-start;
      width:2.5rem;
      justify-content:center;
      @media screen and (min-width: 768px){
        width: 2rem;
      }
      i {
        font-style:normal;
        font-size:.7rem;
        color:${props => props.theme.colors.gray};
        margin-left:0.1rem;
        transform:translateY(-5%);
      }
      &:last-child {
        color:${props => props.theme.colors.gray}
      }
    }
  `;
export default LastDays;