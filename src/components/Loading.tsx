import React from 'react';
import styled from 'styled-components';
import loading from '../icons/loading.svg';

const Loading: React.FC = () => {
  return (
    <LoadingWrapper>
      <img src={loading} alt="Loading" />
    </LoadingWrapper>
  )
}
const LoadingWrapper = styled.div`
  width:100%;
  height:100%;
  display:flex;
  align-items:center;
  justify-content:center;
  img {
    max-width:2.6rem;
  }
`;
export default Loading;