import React, {useState} from 'react';
import search from '../icons/search.svg';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import * as yup from 'yup';
import styled from 'styled-components';
import moment from 'moment';
import _ from 'lodash';

let schema = yup.object().shape({
  keyword: yup.string().required()
})

const Search: React.FC<{updateWeather: (weather: any, list: any) => void}> = ({updateWeather}) => {

  const [err, setErr] = useState<string>('');

  const changeValue = (value:any) => {
    getWeather(value, 'address');
  }
  const getWeather = (value: any, type: string) => {
    let url:string = '';
    if(type === 'address'){
      url = `//api.openweathermap.org/data/2.5/weather?q=${value.keyword}&appid=${process.env.REACT_APP_WEATHER_APIKEY}&units=metric`;
    }else if(type === 'geolocation'){
      url = `//api.openweathermap.org/data/2.5/weather?lat=${value.lat}&lon=${value.long}&appid=${process.env.REACT_APP_WEATHER_APIKEY}&units=metric`;
    };
    fetch(url).then(r => {
      if(r.status === 404 || r.status === 500){
        setErr('City not found, please try again');
      }
      return r.json();
    }).then(r => {
      if(r.cod === '404'){
        setErr(r.message + ', please try again');
      }else {
        setErr('');
        fetch(`//api.openweathermap.org/data/2.5/forecast?lat=${r.coord.lat}&lon=${r.coord.lon}&appid=${process.env.REACT_APP_WEATHER_APIKEY}&units=metric`).then(a => a.json()).then(a => {
          let list:any = a.list.filter((r:any) => moment(r.dt_txt).format('DD/MM/YYYY') !== moment().format('DD/MM/YYYY'));
          list.map((x:any) => {
            x.gref = x.dt_txt.split(' ')[0];
            return x;
          })
          let groupedList = _.groupBy(list, 'gref');
          var filteredList:any = [];
          _.forEach(groupedList, (arr: any) => {
            filteredList.push({min: _.reduce(arr, (prev: any, curr: any) => prev.main.temp_min < curr.main.temp_min ? prev : curr), max:_.reduce(arr, (prev: any, curr: any) => prev.main.temp_max > curr.main.temp_max ? prev : curr)})
          })
          updateWeather(r, filteredList);
        })
      }
    });
  }
  return(
  <SearchBar>
    <Formik
    initialValues={{keyword: ''}}
    validate={
      async values => {
        type Errors = {
          keyword: string;
        }
        const err = {} as Errors;
        await schema.isValid(values).then((valid) => {
          if(!valid){
            err.keyword = 'Please type a place to search';
          }
        })
        return err;
      }
    }
    onSubmit={(values) => {
      changeValue(values);
    }}
    >
      {() => (
        <Form>
          <Field type="text" name="keyword" placeholder="Search for City, State, Country" />
          <Button type="submit"><img src={search} alt="Search Icon" /></Button>
          <ErrorMessage name="keyword" component="p" />
          {err ? (<p>{err}</p>) : null}
        </Form>
      )}
    </Formik>
  </SearchBar>
  )
}
const SearchBar = styled.div`
  width:100%;
  position:relative;
  z-index:3;
  input {
    height: 3rem;
    width:100%;
    box-shadow: inset 0px 0px .25rem ${props => props.theme.colors.graylight};
    border:0;
    border-radius:.5rem;
    border:1px solid rgba(${props => props.theme.colors.bluelight}, .4);
    padding:.5rem 2.5rem .5rem 1rem;
    @media screen and (min-width: 768px){
      font-size:0.75rem;
    }
  }
  p {
    font-size:.8rem;
    color:${props => props.theme.colors.gray};
    margin-top:.5rem;
    margin-bottom:.5rem;
    text-align:center;
    &:first-letter {
      text-transform:capitalize;
    }
  }
  @media screen and (min-width: 768px){
    padding:0 1rem;
  }
  `;
  const Button = styled.button`
  position:absolute;
  right:0;
  top:0;
  background:none;
  border:0;
  width:3rem;
  height:3rem;
  @media screen and (min-width: 768px){
    right:1rem;
  }
`;
export default Search;