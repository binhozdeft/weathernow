import React from 'react';
import styled from 'styled-components';
import Loading from './Loading';

const TempMap: React.FC<{weather: any}> = ({weather}) => {
  return (
    <TempMapWrapper>
      {weather ? (
        <div>
          <Title>Temperature Map</Title>
          <img src={`https://tile.openweathermap.org/map/temp_new/3/${(Math.floor((weather.coord.lon+180)/360*Math.pow(2,3)))}/${(Math.floor((1-Math.log(Math.tan(weather.coord.lat*Math.PI/180) + 1/Math.cos(weather.coord.lat*Math.PI/180))/Math.PI)/2 *Math.pow(2,3)))}?appid=${process.env.REACT_APP_WEATHER_APIKEY}`} width="100%" alt="" />
        </div>
      ) : <Loading />}
    </TempMapWrapper>
  )
}
const TempMapWrapper = styled.section`
  width:100%;
  background:white;
  text-align:left;
  padding:1.5rem 0 0;
  background: linear-gradient(225.52deg, rgba(255, 255, 255, 0.8) 0%, rgba(255, 255, 255, 0.5) 74.7%);
  box-shadow: -.25rem .25rem 1.5rem -.1px ${props => props.theme.colors.graylight};
  backdrop-filter: blur(.4rem);
  margin-top:1.5rem;
  border-radius: .5rem;
  min-height:15rem;
  position:relative;
  font-size: .8rem;
  line-height: 1.1rem;
  overflow:hidden;
`;
const Title = styled.h2`
  font-size: .9rem;
  line-height: 1.3rem;
  color:${props => props.theme.colors.blue};
  text-align:center;
  margin-bottom:1rem;
`;
export default TempMap;