import React from 'react';
import styled from 'styled-components';
import Degree from './Degree';
import moment from 'moment';
import place from '../icons/place.svg';
import Loading from './Loading';

const Today: React.FC<{unit: string, value: any}> = ({unit, value}) => {
  return (
    <TodayWrapper>
      {value ? (
        <div>
          <Icon>
            <img src={`https://openweathermap.org/img/wn/${value.weather[0].icon}@2x.png`} alt={value.weather[0].main} />
          </Icon>
          <strong>Today</strong>
          <Date>
            {moment().format('dddd, MMMM Do YYYY')}
          </Date>
          <div className="degree">
            <Degree value={value.main.temp} unit={unit} />
          </div>
          <span className="weather">{value.weather[0].description} • Humidity: {value.main.humidity}%</span>
          <Place><img src={place} alt="Place Icon" /> {value.name} • {value.sys.country}</Place>
          <Temp>
            <p>max
            <Degree value={value.main.temp_max} unit={unit} />
            </p>
            <p>min
            <Degree value={value.main.temp_min} unit={unit} />
            </p>
          </Temp>
        </div>
      ) : <Loading />}
    </TodayWrapper>
  )
}
const TodayWrapper = styled.section`
  width:100%;
  background:white;
  text-align:center;
  padding:1.5rem 1rem;
  background: linear-gradient(225.52deg, rgba(255, 255, 255, 0.8) 0%, rgba(255, 255, 255, 0.5) 74.7%);
  box-shadow: -.25rem .25rem 1.5rem -.1px ${props => props.theme.colors.graylight};
  backdrop-filter: blur(.4rem);
  margin-top:1.5rem;
  border-radius: .5rem;
  min-height:15rem;
  position:relative;
  @media screen and (min-width: 768px){
    box-shadow:none;
    padding:1rem;
  }
  strong {
    font-style: normal;
    font-weight: normal;
    font-size: 1.5rem;
    line-height: 2rem;
    color: ${props => props.theme.colors.blue};
  }
  .degree {
    transform:translateX(.8rem);
    span {
      font-size: 6rem;
      line-height: 6rem;
      color: ${props => props.theme.colors.blue};
      display:flex;
      justify-content:center;
      i {
        font-style:normal;
        font-size: 1rem;
        line-height: 1.5rem;
        color: ${props => props.theme.colors.gray};
        margin-top:.5rem;
      }
    }
  }
  .weather {
    text-align:center;
    font-size: .8rem;
    color: ${props => props.theme.colors.gray};
    margin-top:.5rem;
    margin-bottom:.5rem;
    text-transform:capitalize;
  }
`;
const Date = styled.div`
  text-align:center;
  font-size: .8rem;
  color: ${props => props.theme.colors.blue};
  margin-top:.5rem;
  margin-bottom:.5rem;
  opacity:.6;
`;
const Place = styled.p`
  text-align:center;
  font-size: .9rem;
  line-height: 1rem;
  color: ${props => props.theme.colors.blue};
  margin-top:1rem;
  display:flex;
  align-items:center;
  justify-content:center;
  text-transform:uppercase;
  img {
    margin-right:.3rem;
  }
`;
const Icon = styled.div`
  position:absolute;
  top:-.5rem;
  right:-.5rem;
  background: ${props => props.theme.colors.graylight};
  display:block;
  width:4rem;
  height:4rem;
  overflow:hidden;
  border-radius:4rem;
  box-shadow:-1px 1px 3px ${props => props.theme.colors.gray};
  img {
    object-fit:cover;
  }
`;
const Temp = styled.span`
color:${props => props.theme.colors.blue};
display:flex;
align-items:center;
justify-content:center;
position:absolute;
right:.5rem;
top:50%;
transform:translateY(-50%);
p {
  font-size:0.6rem;
  span {
    font-size:0.75rem;
  }
}
span {
  display:flex;
  align-items:flex-start;
  width:2.5rem;
  justify-content:center;
  i {
    font-style:normal;
    font-size:.7rem;
    color:${props => props.theme.colors.gray};
    margin-left:0.1rem;
    transform:translateY(-5%);
  }
  &:last-child {
    color:${props => props.theme.colors.gray}
  }
}`;
export default Today;